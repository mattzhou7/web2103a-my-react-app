import ReactDOM from 'react-dom/client'
import './index.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import App from './App'
// console.log(styles)
// 根元素
// const root = ReactDOM.createRoot(document.getElementById('root'))
// root.render(<h1>Hello World</h1>)

ReactDOM.createRoot(document.getElementById('root'))
  .render(<App />)

// v17
// ReactDOM.render(
//   <h1>Hello World!</h1>,
//   document.getElementById('root')
// )
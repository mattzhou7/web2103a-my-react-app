const { createProxyMiddleware } = require('http-proxy-middleware');

// According to:
// https://create-react-app.dev/docs/proxying-api-requests-in-development/#configuring-the-proxy-manually
module.exports = function(app) {
  app.use(
    '/chouti-img',
    createProxyMiddleware({
      target: 'https://img3.chouti.com/',
      changeOrigin: true,
      pathRewrite: {
        '^/chouti-img' : '/'
      },
      headers: {
        "Referer": "https://dig.chouti.com/"
      }
    })
  );
};